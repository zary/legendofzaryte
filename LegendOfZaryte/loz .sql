-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Hoszt: 127.0.0.1
-- Létrehozás ideje: 2014. Ápr 18. 17:38
-- Szerver verzió: 5.6.16
-- PHP verzió: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `loz`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `enemies`
--

CREATE TABLE IF NOT EXISTS `enemies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=125 ;

--
-- A tábla adatainak kiíratása `enemies`
--

INSERT INTO `enemies` (`id`, `level`, `type`, `x`, `y`) VALUES
(1, 1, 1, 810, 300),
(2, 1, 1, 1200, 180),
(3, 1, 1, 1440, 120),
(4, 1, 1, 1500, 330),
(5, 1, 1, 1620, 330),
(6, 1, 1, 1770, 300),
(7, 1, 1, 2250, 300),
(8, 1, 1, 2460, 300),
(9, 1, 1, 2700, 300),
(10, 1, 1, 2970, 300),
(11, 1, 1, 2880, 120),
(12, 1, 1, 2550, 90),
(13, 1, 1, 2070, 120),
(14, 1, 2, 1260, 150),
(15, 1, 2, 1020, 30),
(16, 1, 2, 1620, 30),
(17, 1, 2, 1980, 180),
(21, 1, 2, 2070, 180),
(22, 1, 2, 2220, 240),
(23, 1, 2, 2610, 150),
(24, 1, 2, 2880, 30),
(25, 1, 2, 3000, 210),
(26, 1, 3, 2370, 150),
(27, 1, 3, 2340, 30),
(28, 1, 3, 2600, 185),
(29, 2, 1, 50, 100),
(30, 2, 1, 410, 470),
(31, 2, 1, 450, 320),
(32, 2, 1, 680, 140),
(33, 2, 1, 580, 530),
(34, 2, 1, 780, 530),
(35, 2, 1, 990, 530),
(36, 2, 1, 1700, 440),
(37, 2, 1, 1460, 560),
(38, 2, 1, 860, 200),
(39, 2, 1, 1525, 200),
(40, 2, 1, 1880, 200),
(41, 2, 1, 2320, 560),
(42, 2, 1, 2500, 500),
(43, 2, 1, 2800, 440),
(44, 2, 1, 1800, 200),
(45, 2, 2, 630, 410),
(46, 2, 2, 165, 70),
(47, 2, 2, 1780, 340),
(48, 2, 2, 2870, 490),
(49, 2, 2, 480, 380),
(50, 2, 2, 1190, 520),
(51, 2, 2, 1460, 310),
(52, 2, 2, 1730, 400),
(53, 2, 3, 350, 80),
(54, 2, 3, 1490, 200),
(55, 2, 3, 2540, 330),
(56, 2, 3, 2050, 50),
(57, 3, 1, 470, 350),
(58, 3, 1, 700, 350),
(59, 3, 1, 950, 350),
(60, 3, 1, 1050, 80),
(61, 3, 1, 1200, 250),
(62, 3, 1, 1590, 260),
(63, 3, 1, 2110, 200),
(64, 3, 1, 2130, 350),
(65, 3, 1, 2880, 350),
(66, 3, 1, 3050, 350),
(67, 3, 1, 3100, 350),
(68, 3, 2, 775, 40),
(69, 3, 2, 840, 40),
(70, 3, 2, 1480, 40),
(71, 3, 2, 2030, 130),
(72, 3, 2, 2210, 290),
(73, 3, 2, 2390, 100),
(74, 3, 2, 2570, 60),
(75, 3, 3, 100, 70),
(76, 3, 3, 315, 200),
(77, 3, 3, 1530, 100),
(78, 3, 3, 1790, 70),
(79, 3, 3, 2500, 250),
(80, 3, 7, 2600, 350),
(81, 4, 1, 150, 210),
(83, 4, 1, 150, 360),
(84, 4, 1, 180, 540),
(85, 4, 1, 510, 300),
(86, 4, 1, 420, 210),
(87, 4, 1, 630, 60),
(88, 4, 1, 690, 60),
(89, 4, 1, 930, 210),
(90, 4, 1, 990, 450),
(91, 4, 1, 1020, 60),
(92, 4, 1, 1200, 120),
(93, 4, 1, 1530, 270),
(94, 4, 1, 1830, 90),
(95, 4, 1, 2010, 450),
(96, 4, 1, 2190, 300),
(97, 4, 1, 2400, 120),
(98, 4, 2, 270, 180),
(99, 4, 2, 270, 480),
(100, 4, 2, 450, 450),
(101, 4, 2, 570, 270),
(102, 4, 2, 390, 30),
(103, 4, 2, 1170, 30),
(104, 4, 2, 1290, 510),
(105, 4, 2, 1500, 330),
(106, 4, 2, 1740, 210),
(107, 4, 2, 1740, 30),
(108, 4, 2, 2130, 420),
(109, 4, 2, 2220, 420),
(110, 4, 2, 2310, 420),
(111, 4, 2, 2490, 270),
(112, 4, 2, 2430, 300),
(113, 4, 4, 120, 60),
(114, 4, 4, 60, 360),
(115, 4, 4, 750, 210),
(116, 4, 4, 870, 60),
(117, 4, 4, 840, 450),
(118, 4, 4, 1230, 210),
(119, 4, 4, 1260, 60),
(120, 4, 4, 1590, 210),
(121, 4, 4, 2010, 60),
(122, 4, 4, 2160, 300),
(123, 4, 4, 2520, 330),
(124, 4, 8, 2950, 520);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `enemy_types`
--

CREATE TABLE IF NOT EXISTS `enemy_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=9 ;

--
-- A tábla adatainak kiíratása `enemy_types`
--

INSERT INTO `enemy_types` (`id`, `name`) VALUES
(1, 'Slugger'),
(2, 'Spider'),
(3, 'Bird'),
(4, 'Bat'),
(5, 'GroundSpider'),
(6, 'HavasBird'),
(7, 'SnowSpider'),
(8, 'Wizard');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `highscores`
--

CREATE TABLE IF NOT EXISTS `highscores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `point` int(11) NOT NULL,
  `level` varchar(10) COLLATE utf8_hungarian_ci NOT NULL,
  `gem` int(11) NOT NULL,
  `datum` date NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `levels`
--

CREATE TABLE IF NOT EXISTS `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=5 ;

--
-- A tábla adatainak kiíratása `levels`
--

INSERT INTO `levels` (`id`, `name`) VALUES
(1, 'Level 1'),
(2, 'Level 2'),
(3, 'Level 3'),
(4, 'Level 4');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
