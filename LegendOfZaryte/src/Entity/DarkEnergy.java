package Entity;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import TileMap.TileMap;

public class DarkEnergy extends Enemy {
	
	private BufferedImage[] sprites;
	
	private boolean start;
	private boolean permanent;
	private boolean remove;
	private int type = 0;
	public static int VECTOR = 0;
	public static int GRAVITY = 1;
	public static int BOUNCE = 2;
	
	private int bounceCount = 0;
	
	public DarkEnergy(TileMap tm) {
		
		super(tm);

		width = 30;
		height = 30;
		cwidth = 10;
		cheight = 10;
		
		damage = 1;
		moveSpeed = 5;
		
		/*startSprites = DarkEnergy[0];
		sprites = Entity.DarkEnergy[1];*/
		// load sprites
				try {
					
					BufferedImage spritesheet = ImageIO.read(
						getClass().getResourceAsStream(
							"/Sprites/Player/fireball.gif"
						)
					);
					
					sprites = new BufferedImage[4];
					for(int i = 0; i < sprites.length; i++) {
						sprites[i] = spritesheet.getSubimage(
							i * width,
							0,
							width,
							height
						);
					}
					/*sprites2 = new BufferedImage[4];
					for(int i = 0; i < sprites.length; i++) {
						sprites[i] = spritesheet.getSubimage(
							i * width,
							height,
							width,
							height
						);
					}*/
					
					
					
					
				}		catch(Exception e) {
					e.printStackTrace();
				}
		start = true;
		flinching = true;
		permanent = false;
		animation = new Animation();
		animation.setFrames(sprites);
		animation.setDelay(70);
	}
	
	public void setType(int i) { type = i; }
	public void setPermanent(boolean b) { permanent = b; }
	public boolean shouldRemove() { return remove; }
	public void update() {
		
		if(start) {
			if(animation.hasPlayedOnce()) {
				animation.setFrames(sprites);
				animation.setDelay(70);
				start = false;
				
			}
		}
		
		if(type == VECTOR) {
			x += dx;
			y += dy;
			
		}
		else if(type == GRAVITY) {
			dy += 0.2;
			x += dx;
			y += dy;
		}
		else if(type == BOUNCE) {
			double dx2 = dx;
			double dy2 = dy;
			checkTileMapCollision();
			if(dx == 0) {
				dx = -dx2;
				bounceCount++;
			}
			if(dy == 0) {
				dy = -dy2;
				bounceCount++;
			}
			x += dx;
			y += dy;
		}
		
		// update animation
		animation.update();
		checkTileMapCollision();
		
		if(!permanent) {
		//System.out.println("X: "+x+" Y: "+y);
			//System.out.println(tileMap.getWidth() + "  "+tileMap.getHeight());
			if(x < 2690 || x > 2955 || y < 0 || y > tileMap.getHeight()) {
				remove = true;
				

			}
			if(bounceCount == 3) {
				remove = true;
			}
		}
		
	}
	
	public void draw(Graphics2D g) {
		super.draw(g);
		setMapPosition();
	}
	
}
