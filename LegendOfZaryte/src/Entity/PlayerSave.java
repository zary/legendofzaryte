package Entity;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;


public class PlayerSave {
	//stats
	public static int fullhp=3;
	public static int health=5;
	public static int maxHealth=5;
	
	//attributes
	public static double fire=1500;
	public static int maxFire=1500;
	public static int fireBallDamage=5;
	public static double addFire=0; //recharge rate of fire
	public static int scratchDamage=8;
	public static int scratchRange=40;
	
	//statistic
	public static int killedDog=0;
	public static int gemCount=0;
	public static int currentLeveL=1;
	public static int time=0;
	
	public static int getLives() { return fullhp; }
	public static void setLives(int i) { fullhp = i; }
	
	public static int getKilledDog() {return killedDog;}
	public static void increaseKilledDog() { killedDog++;}
	
	public static int getHealth() { return health; }
	public static void setHealth(int i) { health = i; }
	public static Timer ido=new Timer (1000, new Counter());
	
	static class Counter implements ActionListener {
		public void actionPerformed (ActionEvent e) {
			time++;
		}
	}
	
}
