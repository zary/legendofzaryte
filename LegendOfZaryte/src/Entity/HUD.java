package Entity;
import Main.GamePanel;
import TileMap.TileMap;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

public class HUD {
	
	private BufferedImage image,gem1,gem2,gem3,gem4,gem5;
	private Font font,font2;
	private Player player;
	
	public HUD (Player p) {
		player = p;
		try {
			image = ImageIO.read(getClass().getResourceAsStream("/HUD/hud.gif"));
			font = new Font ("Arial" , Font.PLAIN, 14);
			font2 =new Font( "Arial", Font.PLAIN, 10);
			}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public void Gem1HUD () {
		try {
			gem1 = ImageIO.read(getClass().getResourceAsStream("/HUD/gem1.png"));
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public void Gem2HUD () {
		try {
			gem2 = ImageIO.read(getClass().getResourceAsStream("/HUD/gem2.png"));
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public void Gem3HUD () {
		try {
			gem3 = ImageIO.read(getClass().getResourceAsStream("/HUD/gem3.png"));
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public void Gem4HUD () {
		try {
			gem4 = ImageIO.read(getClass().getResourceAsStream("/HUD/gem4.png"));
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public void Gem5HUD () {
		try {
			gem5 = ImageIO.read(getClass().getResourceAsStream("/HUD/gem5.png"));
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	public void draw (Graphics2D g) {
		g.drawImage(image, 0, 0,null);
		g.drawImage(gem1,100,10,null); // egym�s melletire kell �llitani!
		g.drawImage(gem2,110,10,null);
		g.drawImage(gem3,120,10,null);
		g.drawImage(gem4,130,10,null);
		g.drawImage(gem5,140,10,null);
		g.setFont(font);
		g.setColor(Color.white);
		g.drawString(player.getHealth() +"/" + player.getMaxHealth(),25 ,35);
		g.drawString((int)player.getFire() /100 +"/" + player.getMaxFire()/100,25 ,56);
		g.drawString(String.valueOf("x "+Player.fullhp),25,15);		
		g.drawString("Kill Count: "+PlayerSave.getKilledDog(),GamePanel.WIDTH-95,GamePanel.HEIGHT-225);
		if (Player.toggleInfo) {
			g.setFont(font2);
			g.drawString("All gempa: "+String.valueOf(PlayerSave.gemCount),GamePanel.WIDTH-80,GamePanel.HEIGHT-50);
			g.drawString("FireBall dmg: "+String.valueOf(Player.fireBallDamage),GamePanel.WIDTH-80,GamePanel.HEIGHT-40);
			g.drawString("Scratch dmg: "+String.valueOf(Player.scratchDamage),GamePanel.WIDTH-80,GamePanel.HEIGHT-30);
			g.drawString("Scratch rng: "+String.valueOf(Player.scratchRange),GamePanel.WIDTH-80,GamePanel.HEIGHT-20);
			g.drawString("FB recharge: "+String.valueOf(Player.addFire),GamePanel.WIDTH-80,GamePanel.HEIGHT-10);
			g.drawString("Curr LVL: "+String.valueOf(PlayerSave.currentLeveL),GamePanel.WIDTH-80,GamePanel.HEIGHT);
			
		}
		if (Player.toggleHPbar) {
			
			int healthBarWidth =50;
			int healthBarHeight=5;
			
			float healthScale = (float)Player.health / (float)Player.maxHealth;
	
			g.setColor(Color.red);
			g.fillRect((int) ((int)player.x+TileMap.getx())-25, (int) (player.gety()+TileMap.gety()-25),healthBarWidth,healthBarHeight);
			
		    g.setColor(Color.GREEN);
		    g.fillRect((int) ((int)player.x+TileMap.getx())-25, (int) (player.gety()+TileMap.gety()-25), (int) (healthBarWidth * healthScale), healthBarHeight);
		}
	}

}
