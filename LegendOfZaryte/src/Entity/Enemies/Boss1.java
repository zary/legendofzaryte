package Entity.Enemies;

import Entity.*;

import TileMap.TileMap;



import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.imageio.ImageIO;


public class Boss1 extends Enemy {
	
	private Player player;
	private ArrayList<Enemy> enemies;
	
	private BufferedImage[] spritesI,spritesA;
	private static final int IDLE = 0;
	private static final int ATTACKING = 2;
	
	private int attackTick;
	private int attackDelay = 50;
	private int step;
	DarkEnergy de;
	public Boss1(TileMap tm, Player p, ArrayList<Enemy> en) {
		
		
		super(tm);
		player = p;
		maxHealth=3000;
		health = 3000;
		enemies = en;
		width = 70;
		height = 50;
		cwidth = 30;
		cheight = 40;
		
		damage = 1;
		moveSpeed = 1.5;
		fallSpeed = 0.15;
		maxFallSpeed = 4.0;
		jumpStart = -5;
		// load sprites
				try {
					BufferedImage spritesheet = ImageIO.read(
						getClass().getResourceAsStream(
							"/Sprites/Boss/wizardTile.png"
						)
					);

						spritesI = new BufferedImage[2];
						for(int i = 0; i < spritesI.length; i++) {
							spritesI[i] = spritesheet.getSubimage(
								i * width,
								0,
								width,
								height
							);
						}
						
						spritesA = new BufferedImage[2];
						for(int i = 0; i < spritesA.length; i++) {
							spritesA[i] = spritesheet.getSubimage(
								i * width,
								height,
								width,
								height
							);
						}
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				animation = new Animation();
				animation.setFrames(spritesI);
				animation.setDelay(700);
				facingRight=true;
				attackTick = 0;
	}

	public void update() {
		if (health<=2000) attackDelay=150;
		if (health<=1500) attackDelay=100;
		if (health<=1000) attackDelay=75;
		
		// check if done flinching
				if(flinching) {
					flinchCount++;
					if(flinchCount == 6) flinching = false;
				}
				
				if(player.getx() < x) facingRight = true;
				else facingRight = false;
				checkTileMapCollision();
				setPosition(xtemp, ytemp);
				
				// update animation
				animation.update();
				
				
				// idle
				if(step == 0) {
					
					if(currentAction != IDLE) {
						currentAction = IDLE;
						animation.setDelay(-1);
					}
					attackTick++;
					if(attackTick >= attackDelay && Math.abs(player.getx() - x) < 300) {
						step++;
						attackTick = 0;
					}
				}
				// attack
				if(step == 1) {
					if(currentAction != ATTACKING) {
						currentAction = ATTACKING;
						animation.setFrames(spritesA);
						animation.setDelay(700);
						de = new DarkEnergy(tileMap);
						de.setPosition(x, y);	
						if(facingRight) de.setVector(-3, 0);
						else de.setVector(3, 0);
						enemies.add(de);
						
					}
					if(currentAction == ATTACKING && animation.hasPlayedOnce()) {
						step++;
						currentAction = 0;
					}
				}
				
				// done attacking
				if(step == 2) {
					for(int i = 0; i < enemies.size(); i++){
						if(de.shouldRemove()){
							enemies.remove(de);
						}
					}
					if(dy == 0) step++;
				}
				// land
				if(step == 3) {
					step = 0;
					left = right = false;
				}
				
				if (attackTick > 300 || health < 2000) {
					setPosition(2700, 520);
					if (attackTick > 300 || health < 1500) {
						setPosition(2950, 520);
					}
					if (attackTick > 300 || health < 1000) {
						setPosition(2700, 520);
					}
					if (attackTick > 300 || health < 750) {
						setPosition(2950, 520);
					}
					if (attackTick > 300 || health < 500) {
						setPosition(2700, 520);
					}
					if (attackTick > 300 || health < 300) {
						setPosition(2950, 520);
					}
					if (attackTick > 300 || health < 100) {
						setPosition(2700, 520);
					}
					if (attackTick > 300 || health <50) {
						setPosition(2950, 520);
					}
					if (attackTick > 300 || health < 10) {
						setPosition(2700, 520);
					}
				}
				animation.update();

			}
		
	public void draw(Graphics2D g) {
		if(flinching) {
			if(flinchCount == 0 || flinchCount == 2) return;
		}		
		
		setMapPosition();
		super.draw(g);
		
	}
	
}
