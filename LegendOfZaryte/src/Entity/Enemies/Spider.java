package Entity.Enemies;

import Entity.*;
import TileMap.TileMap;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;

public class Spider extends Enemy {
	
	private BufferedImage[] sprites,sprites2,sprites3;
	int x1,y1;
	private boolean once=true;
	public Spider(TileMap tm) {
		
		super(tm);
		
		moveSpeed = 1;
		maxSpeed = 0.3;
		fallSpeed = 0.2;
		maxFallSpeed = 10.0;
		
		width = 30;
		height = 30;
		cwidth = 20;
		cheight = 20;
		hatar=100;
		health = maxHealth = 20;
		damage = 1;
		// load sprites
		try {
			
			BufferedImage spritesheet = ImageIO.read(
				getClass().getResourceAsStream(
					"/Sprites/Enemies/arachnik.gif"
				)
			);
			
			sprites = new BufferedImage[1];
			for(int i = 0; i < sprites.length; i++) {
				sprites[i] = spritesheet.getSubimage(
					i * width,
					0,
					width,
					height
				);
			}
			sprites2 = new BufferedImage[1];
			for(int i = 0; i < sprites.length; i++) {
				sprites2[i] = spritesheet.getSubimage(
					i * width,
					30,
					width,
					height
				);
			}
			sprites3 = new BufferedImage[1];
			for(int i = 0; i < sprites.length; i++) {
				sprites3[i] = spritesheet.getSubimage(
					i * width,
					60,
					width,
					height
				);
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		animation = new Animation();
		animation.setFrames(sprites);
		animation.setDelay(300);
		
		down = true;
		facingRight = true;
		if (PlayerSave.currentLeveL==1) {
			maxSpeed = 0.3;
			moveSpeed = 1;
			animation.setFrames(sprites);
			}
			else if (PlayerSave.currentLeveL==2) { 
				maxSpeed= 0.5;
				moveSpeed =1.2;
				animation.setFrames(sprites); 
			}
			else if (PlayerSave.currentLeveL==3) {
				maxSpeed = 0.6;
				moveSpeed =1.4;
				health =30;
				maxHealth =30;
				animation.setFrames(sprites3);
				}
			else if (PlayerSave.currentLeveL==4) {
				maxSpeed = 1;
				moveSpeed = 2.5;
				health =35;
				maxHealth =35;
					animation.setFrames(sprites2);
					}
		
	}
	
	private void getNextPosition() {
		if (once){
		x1=(int) x;
		y1=(int) y;
		once=false;}
		// movement
		if(up) {
			dy -= moveSpeed;
			if(dy < -maxSpeed) {
				dy = -maxSpeed;
			}
		}
		else if(down) {
			
			dy += moveSpeed;
			if(dy > maxSpeed) {
				dy = maxSpeed;
			}
		}
		
	}
	public void setY(int setY) {
		hatar=setY;
	}
	public void update() {
		
		// update position
		getNextPosition();
		checkTileMapCollision();
		setPosition(xtemp, ytemp);
		
		// check flinching
		if(flinching) {
			long elapsed =
				(System.nanoTime() - flinchTimer) / 1000000;
			if(elapsed > 400) {
				flinching = false;
			}
		}
		// if it hits a wall, go other direction
		if(down && dy == 0 || y>hatar) {
			down = false;
			up = true;
		}
		
		else if(up && dy == 0) {
			down = true;
			up = false;
		}

		// update animation
		animation.update();
		
	}
	
	public void draw(Graphics2D g) {
		setMapPosition();
		
		super.draw(g);
		
	}
	
}


