package Entity.Enemies;

import Entity.*;
import TileMap.TileMap;




import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.imageio.ImageIO;


public class Bunny extends Enemy {
	
	private Player player;


	private ArrayList <BufferedImage[]> sprites;
	private final int[] numFrames = {
			1,3,3
		};
	private int count=0;
	private boolean jumping;
	
	private static final int IDLE = 0;
	private static final int JUMPING = 1;
	private static final int ATTACKING = 2;
	
	private int attackTick;
	private int attackDelay = 200;
	private int step;
	
	public Bunny(TileMap tm, Player p) {

		super(tm);
		player = p;
		maxHealth=1500;
		health = 1500;
		
		width = 30;
		height = 30;
		cwidth = 20;
		cheight = 26;
		
		damage = 1;
		moveSpeed = 1.5;
		fallSpeed = 0.15;
		maxFallSpeed = 4.0;
		jumpStart = -5;
		// load sprites
				try {
					BufferedImage spritesheet = ImageIO.read(
						getClass().getResourceAsStream(
							"/Sprites/Enemies/bunny2.gif"
						)
					);
					sprites = new ArrayList<BufferedImage[]>();
					for(int i = 0; i < 3; i++) {
						BufferedImage[] bi =
								new BufferedImage[numFrames[i]];
							
							for(int j = 0; j < numFrames[i]; j++) {							
								bi[j] = spritesheet.getSubimage(
											j * width,
											i * height,
											width,
											height
									);
							}
							sprites.add(bi);
					}
					
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				animation = new Animation();
				animation.setFrames(sprites.get(IDLE));
				animation.setDelay(100);
				
				attackTick = 0;
	}
	
	private void getNextPosition() {
		if(left) dx = -moveSpeed;
		else if(right) dx = moveSpeed;
		else dx = 0;
		if(falling) {
			dy += fallSpeed;
			if(dy > maxFallSpeed) dy = maxFallSpeed;
		}
		if(jumping && !falling) {
			dy = jumpStart;
		}
	
	}
	public void update() {	
		if (health<=1000) attackDelay=150;
		if (health<=750) attackDelay=100;
		if (health<=500) attackDelay=75;

		// check if done flinching
				if(flinching) {
					flinchCount++;
					if(flinchCount == 6) flinching = false;
				}
				
				getNextPosition();
				checkTileMapCollision();
				setPosition(xtemp, ytemp);
				
				// update animation
				animation.update();
				
				if(player.getx() < x) facingRight = true;
				else facingRight = false;
				
				// idle			
				if(step == 0) {
					
					if(currentAction != IDLE) {
						currentAction = IDLE;
						animation.setFrames(sprites.get(IDLE));
						animation.setDelay(400);
						width= 30;					
					}
					attackTick++;
					if(attackTick >= attackDelay && Math.abs(player.getx() - x) < 160) {
					
						step++;
						attackTick = 0;
					}					
				}

				// jump away
				if(step == 1) {
					if(currentAction != JUMPING) {
						currentAction = JUMPING;
						animation.setFrames(sprites.get(JUMPING));
						animation.setDelay(100);
						width=30;
					}
					jumping = true;
					if(facingRight) left = true;
					else right = true;
					if(falling) {
						step++;
					}
				}
				if (player.getx() < 2850) {
					
				}
				// attack
				if(step == 2) {
					if(dy > 0 && currentAction != ATTACKING) {
						currentAction = ATTACKING;
						animation.setFrames(sprites.get(ATTACKING));
						animation.setDelay(3);
						width=30;
						//Miut�n a boss elindult aszoba bez�rodik
						TileMap.setMap(4, 96, 22);
						
						TileMap.setMap(5, 95, 18);
						TileMap.setMap(5, 96, 22);
						TileMap.setMap(5, 97, 4);
						
						TileMap.setMap(6, 95, 3);
						TileMap.setMap(6, 96, 22);
						TileMap.setMap(6, 97, 4);				
							
						TileMap.setMap(7, 96, 21);
						if (x<player.getx()) {
							facingRight=false;
							}
						else {facingRight=true;
						}

					}
					if(currentAction == ATTACKING) {
						step++;
						currentAction = JUMPING;
						animation.setFrames(sprites.get(JUMPING));
						animation.setDelay(100);
						width=30;
						
					}
				}
				// done attacking
				if(step == 3) {
					if(dy == 0 )
						currentAction = IDLE;
						step++;
				}
				// land
				if(step == 4) {					
					left = right = jumping = false;
					System.out.println(count);
					step = 0;
				}			
				animation.update();
			}
		
	public void draw(Graphics2D g) {
		if(flinching) {
			if(flinchCount == 0 || flinchCount == 2) return;
		}
		setMapPosition();
		super.draw(g);
		
	}
	
}
