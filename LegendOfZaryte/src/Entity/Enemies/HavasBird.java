package Entity.Enemies;

import Entity.*;

import GameState.Level1State;
import TileMap.TileMap;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;

public class HavasBird extends Enemy {
	private Player player;
	private BufferedImage[] sprites;
	public int[] spawn=new int[2];

	public HavasBird(TileMap tm,Player p) {
		super(tm);
		player= p;
		fallSpeed = 0.2;
		maxFallSpeed = 10.0;
		width = 30;
		height = 30;
		cwidth = 20;
		cheight = 20;
		health = maxHealth = 35;
		damage = 3;
		// load sprites lvl 1
				try {
					
					BufferedImage spritesheet = ImageIO.read(
						getClass().getResourceAsStream(
							"/Sprites/Enemies/havasBirdTile.png"
						)
					);
					
					sprites = new BufferedImage[4];
					for(int i = 0; i < sprites.length; i++) {
						sprites[i] = spritesheet.getSubimage(
							i * width,
							0,
							width,
							height
						);
					}
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				
				animation = new Animation();
				animation.setDelay(100);

				facingRight = false;
				
		
		
		maxSpeed = 0.4;
		moveSpeed = 0.4;
		animation.setFrames(sprites);
				
	}
	
	public void update() {
		// check flinching
		if(flinching) {
			long elapsed =
				(System.nanoTime() - flinchTimer) / 1000000;
			if(elapsed > 400) {
				flinching = false;
			}
		}
		
		checkTileMapCollision();
		calculateCorners(x, ydest + 1);
		if(!bottomLeft) {
			left = false;
			right = facingRight = true;
		}
		if(!bottomRight) {
			left = true;
			right = facingRight = false;
		}
		setPosition(xtemp, ytemp);
		
		if(dx == 0) {
			left = !left;
			right = !right;
			facingRight = !facingRight;
		}
		//Player Fel� mozg�s
		if(player.getx() < x){ 
			facingRight = false;
			dx = -moveSpeed;
			}

		if (player.getx() > x ) {
			 facingRight = true;
			dx =moveSpeed;
			}
		if (player.gety() < y) {
			dy = -moveSpeed;
		}
		if (player.gety() > y) {
			dy = moveSpeed;
		}
		////////////Ha a player a mad�r egy sugar�n bel�l van az t�mad, egy�bk�nt vissza megy az eredeti hely�re/////////////////////
			///Balrol k�zel�t�s
		if (Level1State.Espawn[0] - player.getx()>=150 ) {
			
			right = facingRight=true;
			dx = moveSpeed;
	
			if (x <= Level1State.Espawn[0]){
				
				dx = moveSpeed;
			}
			else  dx = -moveSpeed;
		
			if(y <= Level1State.Espawn[1]) {
				dy = moveSpeed;
			}
			else dy = -moveSpeed;
			}		
			//Jobbr�l k�zel�t�s
		if (Level1State.Espawn[0] - player.getx()<=(-150)  ) {
		
			right = facingRight=false;
			dx = -moveSpeed;
			
	
		if (x <= Level1State.Espawn[0]){

			dx = moveSpeed;
		}
		else  dx = -moveSpeed;
		if(y <= Level1State.Espawn[1]) {
			dy = moveSpeed;
		}
		else dy = -moveSpeed;
		}
		animation.update();
	
}
	
	public void draw(Graphics2D g) {
		
		setMapPosition();
		
		super.draw(g);
		
	}
	
}
