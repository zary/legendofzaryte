package Entity.Enemies;

import Entity.*;

import TileMap.TileMap;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;

public class Slugger extends Enemy {
	
	private BufferedImage[] sprites,sprites2,sprites3,sprites4;
	
	public Slugger(TileMap tm) {
		super(tm);
		fallSpeed = 0.2;
		maxFallSpeed = 10.0;
		width = 30;
		height = 30;
		cwidth = 20;
		cheight = 20;
		health = maxHealth = 10;
		damage = 1;
		// load sprites lvl 1
				try {//lvl 1
					
					BufferedImage spritesheet = ImageIO.read(
						getClass().getResourceAsStream(
							"/Sprites/Enemies/slugger.gif"
						)
					);
					
					sprites = new BufferedImage[3];
					for(int i = 0; i < sprites.length; i++) {
						sprites[i] = spritesheet.getSubimage(
							i * width,
							0,
							width,
							height
						);
					}
					sprites2 = new BufferedImage[3];
					for(int i = 0; i < sprites.length; i++) {
						sprites2[i] = spritesheet.getSubimage(
							i * width,
							height,
							width,
							height
						);
					}
					sprites3 = new BufferedImage[3];		
					for(int i = 0; i < sprites.length; i++) {
						sprites3[i] = spritesheet.getSubimage(
							i * width,
							height+30,
							width ,
							height
						);
					}
					sprites4 = new BufferedImage[3];		
					for(int i = 0; i < sprites.length; i++) {
						sprites4[i] = spritesheet.getSubimage(
							i * width,
							height+60,
							width ,
							height
						);
					}
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				
				animation = new Animation();
				animation.setDelay(300);
				
				right = true;
				facingRight = true;
				
		
		if (PlayerSave.currentLeveL==1) {
		maxSpeed = 0.3;
		moveSpeed = 0.3;
		animation.setFrames(sprites);
		}
		else if (PlayerSave.currentLeveL==2) { //gyorsabb
			maxSpeed= 0.5;
			moveSpeed = 0.5;
			animation.setFrames(sprites4); // m�s skin
			health=15;
		}
		else if (PlayerSave.currentLeveL==3) {
			maxSpeed = 0.7;
			moveSpeed = 0.7;
			damage=2;
			health =18;
			maxHealth =20;
			animation.setFrames(sprites3);
			}
		else if (PlayerSave.currentLeveL==4) {
			maxSpeed = 0.9;
			moveSpeed = 0.9;
			damage=3;
			health =20;
			maxHealth =25;
				animation.setFrames(sprites2);
				}
		
	}
	
	private void getNextPosition() {
		if(left) dx = -moveSpeed;
		else if(right) dx = moveSpeed;
		else dx = 0;
		if(falling) {
			dy += fallSpeed;
			if(dy > maxFallSpeed) dy = maxFallSpeed;
		}
		if(jumping && !falling) {
			dy = jumpStart;
		}
		
	}

	public void update() {
		// check flinching
		if(flinching) {
			long elapsed =
				(System.nanoTime() - flinchTimer) / 1000000;
			if(elapsed > 400) {
				flinching = false;
			}
		}
		
		getNextPosition();
		checkTileMapCollision();
		calculateCorners(x, ydest + 1);
		if(!bottomLeft) {
			left = false;
			right = facingRight = true;
		}
		if(!bottomRight) {
			left = true;
			right = facingRight = false;
		}
		setPosition(xtemp, ytemp);
		
		if(dx == 0) {
			left = !left;
			right = !right;
			facingRight = !facingRight;
		}
		
		// update animation
		animation.update();
	
	}
	
	public void draw(Graphics2D g) {
		setMapPosition();
		
		super.draw(g);
		
	}
	
}
