package Entity.Enemies;

import Entity.*;
import TileMap.TileMap;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;

public class Bird extends Enemy {
	
	private BufferedImage[] sprites;
	private int tick;
	private double a;
	private double b;
	private Player player;
	public Bird(TileMap tm,Player p) {
		super(tm);
		player=p;
		width = 30;
		height = 30;
		cwidth = 20;
		cheight = 20;
		health = maxHealth = 10;
		damage = 2;
		tick = 0;
		a = Math.random() * 0.06 + 0.001;
		b = Math.random() * 0.06 + 0.001;
		// load sprites
		try {
			BufferedImage spritesheet = ImageIO.read(
				getClass().getResourceAsStream(
					"/Sprites/Enemies/birdTile.gif"
				)
			);
			sprites = new BufferedImage[4];
			for(int i = 0; i < sprites.length; i++) {
				sprites[i] = spritesheet.getSubimage(
					i * width,
					0,
					width,
					height
				);
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		animation = new Animation();
		animation.setFrames(sprites);
		animation.setDelay(100);
		facingRight = false;
		if (PlayerSave.currentLeveL==1) {

			animation.setFrames(sprites);
			}
			else if (PlayerSave.currentLeveL==2) { //gyorsabb
				health = maxHealth =10;
				animation.setFrames(sprites);
				}
			else if (PlayerSave.currentLeveL==3) {

				health = maxHealth =15;
				damage = 3;
				animation.setFrames(sprites);
				}
			else if (PlayerSave.currentLeveL==1) {

				health = maxHealth =20;
				damage = 4;
					animation.setFrames(sprites);
					}
	}
	
	private void getNextPosition() {

		// movement
		tick++;
		x = Math.sin(a * tick) + x;
		y = Math.sin(b * tick) + y;
		
	}
	public void update() {
		
		// update position
		getNextPosition();
		checkTileMapCollision();
		setPosition(xtemp, ytemp);
		
		// check flinching
		if(flinching) {
			long elapsed =
				(System.nanoTime() - flinchTimer) / 1000000;
			if(elapsed > 400) {
				flinching = false;
			}
		}
		// update animation
		animation.update(); 
		
		//update facing
		
		if (x<player.getx()) {
			facingRight=true;
			}
		else {facingRight=false;}
	}
	
	public void draw(Graphics2D g) {
		setMapPosition();
		super.draw(g);
	}
	
}
