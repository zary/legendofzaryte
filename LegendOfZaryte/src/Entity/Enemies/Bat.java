package Entity.Enemies;

import Entity.*;
import TileMap.TileMap;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;

public class Bat extends Enemy {
	
	private BufferedImage[] sprites;
	private int tick;
	private double a;
	private double b;
	private Player player;
	public Bat(TileMap tm,Player p) {
		super(tm);
		player=p;
		width = 30;
		height = 30;
		cwidth = 20;
		cheight = 20;
		health = maxHealth = 10;
		damage = 2;
		tick = 0;
		a = Math.random() * 0.06 + 0.07;
		b = Math.random() * 0.06 + 0.07;
		// load sprites
		try {
			BufferedImage spritesheet = ImageIO.read(
				getClass().getResourceAsStream(
					"/Sprites/Enemies/bat.gif"
				)
			);
			sprites = new BufferedImage[3];
			for(int i = 0; i < sprites.length; i++) {
				sprites[i] = spritesheet.getSubimage(
					i * width,
					0,
					width,
					height
				);
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		animation = new Animation();
		animation.setFrames(sprites);
		animation.setDelay(100);
		facingRight = false;
		if (PlayerSave.currentLeveL==1) {
			
			animation.setFrames(sprites);
			}
			else if (PlayerSave.currentLeveL==2) { //gyorsabb
				health =15;
				maxHealth=15;
				animation.setFrames(sprites);
			}
			else if (PlayerSave.currentLeveL==3) {

				health =20;
				maxHealth =20;
				animation.setFrames(sprites);
				}
			else if (PlayerSave.currentLeveL==4) {

				health =25;
				maxHealth =25;
					animation.setFrames(sprites);
					}
		
	}
	
	private void getNextPosition() {
		
		// movement
		tick++;
		x = Math.sin(a * tick) + x;
		y = Math.sin(b * tick) + y;
		
		
	}
	public void update() {
		
		// update position
		getNextPosition();
		checkTileMapCollision();
		setPosition(xtemp, ytemp);
		
		// check flinching
		if(flinching) {
			long elapsed =
				(System.nanoTime() - flinchTimer) / 1000000;
			if(elapsed > 400) {
				flinching = false;
			}
		}
		// update animation
		animation.update(); 
		
		//update facing
		
		if (x<player.getx()) {
			facingRight=true;
			}
		else {facingRight=false;}
	}
	
	public void draw(Graphics2D g) {
		setMapPosition();
		super.draw(g);
	}
	
}
