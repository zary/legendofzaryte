package Entity.Enemies;

import Entity.*;
import TileMap.TileMap;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import javax.imageio.ImageIO;

public class GroundSpider extends Enemy {
	
	private BufferedImage[] sprites;
	
	public GroundSpider(TileMap tm) {
		super(tm);
		fallSpeed = 0.2;
		maxFallSpeed = 10.0;
		width = 35;
		height = 30;
		cwidth = 35;
		cheight = 35;
		health = maxHealth = 10;
		damage = 1;
		// load sprites lvl 1
				try {//lvl 1
					
					BufferedImage spritesheet = ImageIO.read(
						getClass().getResourceAsStream(
							"/Sprites/Enemies/groundSpider.png"
						)
					);
					
					sprites = new BufferedImage[6];
					for(int i = 0; i < sprites.length; i++) {
						sprites[i] = spritesheet.getSubimage(
							i * width,
							0,
							width,
							height
						);
					}

				}
				catch(Exception e) {
					e.printStackTrace();
				}
				
				animation = new Animation();
				animation.setDelay(300);
				
				right = true;
				facingRight = true;
				
		
	
		maxSpeed = 0.3;
		moveSpeed = 0.3;
		animation.setFrames(sprites);
				
	}
	
	private void getNextPosition() {
		if(left) dx = -moveSpeed;
		else if(right) dx = moveSpeed;
		else dx = 0;
		if(falling) {
			dy += fallSpeed;
			if(dy > maxFallSpeed) dy = maxFallSpeed;
		}
		if(jumping && !falling) {
			dy = jumpStart;
		}
		
	}

	public void update() {
		// check flinching
		if(flinching) {
			long elapsed =
				(System.nanoTime() - flinchTimer) / 1000000;
			if(elapsed > 400) {
				flinching = false;
			}
		}
		
		getNextPosition();
		checkTileMapCollision();
		calculateCorners(x, ydest + 1);
		if(!bottomLeft) {
			left = false;
			right = facingRight = true;
		}
		if(!bottomRight) {
			left = true;
			right = facingRight = false;
		}
		setPosition(xtemp, ytemp);
		
		if(dx == 0) {
			left = !left;
			right = !right;
			facingRight = !facingRight;
		}
		
		// update animation
		animation.update();
	
	}
	
	public void draw(Graphics2D g) {
		
		//if(notOnScreen()) return;
		checkTileMapCollision();
		setMapPosition();
		
		super.draw(g);
		
	}
	
}
