package Entity.Enemies;

import Entity.*;
import TileMap.TileMap;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;

public class Gems extends Enemy {
	
	private BufferedImage[] sprites;
	
	public Gems(TileMap tm) {
		
		super(tm);
		
		width = 30;
		height = 25;
		cwidth = 20;
		cheight = 20;
		// load sprites
		try {
			
			BufferedImage spritesheet = ImageIO.read(
				getClass().getResourceAsStream(
					"/Gems/gemTiles.png"
				)
			);
			
			sprites = new BufferedImage[6];
			for(int i = 0; i < sprites.length; i++) {
				sprites[i] = spritesheet.getSubimage(
					0,
					i*height,
					width,
					height
				);
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		animation = new Animation();
		animation.setFrames(sprites);
		animation.setDelay(100);
		
		
		right = true;
		facingRight = true;
		
	}
	public void gemChange( int gempa) {
		animation.setFrame(gempa);
	}
	public void update() {
		
		// update position
		checkTileMapCollision();
		setPosition(xtemp, ytemp);

	}
	
	public void draw(Graphics2D g) {
		
		setMapPosition();
		super.draw(g);
	}
	
}
