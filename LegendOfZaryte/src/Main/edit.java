package Main;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
@SuppressWarnings("serial")
public class edit extends JFrame{
	JPanel editorPanel,tilePanel;
	int magassag=20; //p�lya magass�ga
	int hossz=120; //p�lya hossz�s�ga
	int mozgas=0;
	int mozgas2=0;
	int mapid=1;
	JButton[][] gomb=new JButton[magassag][hossz];
	JButton[][] gombTile=new JButton[10][4];
	Button export;
	JFrame tilef;
	String tileint="0";
	String[][] importt=new String[magassag][hossz];
	String[] szet=new String[2];
	ImageIcon tileimg=new ImageIcon("Resources/EditorTiles/0.png");
	TextField save;
	ImageIcon[] tileimp=new ImageIcon[42];
	JTextArea points;
	edit() {
		String importpath=JOptionPane.showInputDialog(editorPanel,"Mi az import�lando file neve?\n(Resources/Maps/)\n(Ha nincs ide ne irj semmit, de az OKra kattints r�!!!)"); //Ha megadsz egy nevet, akkor import�l, am meg nem!
		////TILESET
		tilef=new JFrame("TileSet");
		tilePanel=new JPanel();
		tilef.setBounds(1200,0,260,1000);
		tilePanel.setLayout(new GridLayout(10,4,0,0));
		tilef.add(tilePanel);
		tilef.setLayout(null);
		tilePanel.setBounds(0, 0, 240, 600);
		points=new JTextArea();
		points.setBounds(0, 700, 240, 300);
		tilef.add(points);
		int tileint=-1;
		for (int i=0;i<10;i++) {                
			for (int j=0;j<4;j++) {
				tileint++;
				tileimp[tileint]=new ImageIcon("Resources/EditorTiles/"+tileint+".png");
			
				szet=String.valueOf(tileimp[i]).split("\\.");
				//System.out.println(szet[0]);
				Image img = tileimp[tileint].getImage() ;  
				Image newimg = img.getScaledInstance( 60, 60,  java.awt.Image.SCALE_SMOOTH ) ;  //�tm�retez�s 60*60 pixelre -->Source: stackoverflow
				ImageIcon icon = new ImageIcon( newimg );
				gombTile[i][j]=new JButton(icon);
				gombTile[i][j].setToolTipText(String.valueOf(tileint)); //Tooltipbe van benne a tile sz�ma
				gombTile[i][j].setFocusPainted(false);
				tilePanel.add(gombTile[i][j]);
				gombTile[i][j].addActionListener(new Tiles());
				
			}
		}	 
		//EDITOR
		editorPanel=new JPanel();
		editorPanel.setBounds(mozgas,0,60*hossz,60*magassag);
		add(editorPanel);
		editorPanel.setLayout(new GridLayout(magassag,hossz,0,0)); //grides elrendez�s, hogy ne kelljen egyes�vel beirni a gombok m�ret�t

		for (int i=0;i<magassag;i++) {
			for (int j=0;j<hossz;j++) {
				mapid++;
				Image img = tileimg.getImage() ;  
				Image newimg = img.getScaledInstance( 60, 60,  java.awt.Image.SCALE_SMOOTH ) ;  
				ImageIcon icon = new ImageIcon( newimg );
				gomb[i][j]=new JButton(icon);
				gomb[i][j].setBorderPainted(false);
				gomb[i][j].setToolTipText("0");
				editorPanel.add(gomb[i][j]);
				gomb[i][j].addActionListener(new Map());
			}
		}
		///EXPORT
		export=new Button("Ment�s");
		export.setBounds(110,610,60,25);
		tilef.add(export);
		export.addActionListener(new Export());
		save=new TextField("map.map");
		save.setBounds(10,610,100,25);
		tilef.add(save);
		if (!importpath.isEmpty()) { //ha �res volt a JOptionPane!
		//IMPORT
			String[] sor=new String[5000];
			try {
			    BufferedReader be = new BufferedReader (new FileReader("Resources/Maps/"+importpath));
			    for (int j=0;j<magassag;j++) {
			    	String line = be.readLine();
			    for (int i=0;i<hossz;i++) {
			    	sor = line.split(" ");
			    importt[j][i]=sor[i];
			    }
			    }
			    be.close();
			    } catch (IOException e) {System.out.println(e);}
			 for (int i=0;i<magassag;i++) {
			 	for(int j=0;j<hossz;j++) {
			 		gomb[i][j].setToolTipText(importt[i][j]);
			 			for(int m=0;m<39;m++) {
			 				String[] kivagas=String.valueOf(tileimp[m]).split("\\.");
			 				String kivagas2=kivagas[0];
			 				System.out.println(kivagas2);
			 				kivagas2=kivagas2.substring(22);
			 				System.out.println(kivagas2);
			 				if (gomb[i][j].getToolTipText().equals(kivagas2)) {
			 					
			 					Image img = tileimp[m].getImage() ; 
								Image newimg = img.getScaledInstance( 60, 60,  java.awt.Image.SCALE_SMOOTH ) ;  //�tm�retez�s 60*60 pixelre -->Source: stackoverflow
								ImageIcon icon = new ImageIcon( newimg );
			 					gomb[i][j].setIcon(icon);
			 				}
			 			}
			 		
			 		
			 	}
			 }
			 save.setText(importpath);
		}
		//INITALIZATION
		setLayout(null);
		setTitle("Map Editor for Legend of Zaryte");
		setSize(1200,60*magassag+38);
		setVisible(true);
		tilef.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		editorPanel.addKeyListener(new BillentyuKezeles());
		editorPanel.requestFocus();
	}
 	class BillentyuKezeles implements KeyListener {
  public void keyPressed(KeyEvent e) {
 	switch(e.getKeyCode()) {
		case KeyEvent.VK_RIGHT :  if (mozgas!=60*hossz) {mozgas-=60;editorPanel.setBounds(mozgas,mozgas2,60*hossz,60*magassag);}; break;	
		case KeyEvent.VK_LEFT: if (mozgas!=0) {mozgas+=60;editorPanel.setBounds(mozgas,mozgas2,60*hossz,60*magassag);} break;
		case KeyEvent.VK_UP: mozgas2-=60;editorPanel.setBounds(mozgas,mozgas2,60*hossz,60*magassag); break;
		case KeyEvent.VK_DOWN: mozgas2+=60;editorPanel.setBounds(mozgas,mozgas2,60*hossz,60*magassag); break;
		}
  }
  public void keyReleased(KeyEvent e){ }
  public void keyTyped(KeyEvent e){ }
}
    class Tiles implements ActionListener {
    	public void actionPerformed (ActionEvent e) {
    		tileint=((JButton)e.getSource()).getToolTipText();
    		tileimg=(ImageIcon)((JButton)e.getSource()).getIcon();
    		editorPanel.requestFocus();
    	}
    }
    class Map implements ActionListener {
    	public void actionPerformed (ActionEvent e) {
    		((JButton)e.getSource()).setToolTipText(String.valueOf(tileint));
    		((JButton)e.getSource()).setIcon(tileimg);
    		editorPanel.requestFocus();
    		points.append("new Point ("+((JButton)e.getSource()).getX()/2+","+((JButton)e.getSource()).getY()/2+"),\n");
    		//System.out.println("X: "+((JButton)e.getSource()).getX()/2+" Y: "+((JButton)e.getSource()).getY()/2);
    	}
    }
    class Export implements ActionListener {
    	public void actionPerformed (ActionEvent e) {
    	    PrintWriter printWriter;
			try {
				printWriter = new PrintWriter ("Resources/Maps/"+save.getText());
    	    printWriter.println (hossz);
    	    printWriter.println (magassag);
    	    for (int i=0;i<magassag;i++) {
    	    	for (int j=0;j<hossz;j++) {
    	    		printWriter.print (gomb[i][j].getToolTipText()+" ");
    	    	}
    	    	printWriter.println ("");
    	    }
    	    printWriter.close ();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			editorPanel.requestFocus();
    	}
    }
      
	public static void main(String[] args) {
		new edit();

	}

}
