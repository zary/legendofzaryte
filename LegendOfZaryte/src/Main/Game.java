package Main;

import javax.swing.*;
public class Game {
	public static JFrame window;
	
	public Game() {
		window = new JFrame ("Legend of Zaryte");

		window.setContentPane(new GamePanel());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);
		window.pack();
		window.setVisible(true);
		window.setLocationRelativeTo(null);

		
	}
	public static void main (String[] args) {
		new Game();
	}

}
