package GameState;
 
import Audio.AudioPlayer;
import Entity.PlayerSave;
import Main.Game;
import Main.db;
import TileMap.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

public class GameCompleted extends GameState {
	
	private Background bg;

	private int currentChoice = 0;
	private String[] options = {
		"HighScore",
		"Exit to menu",
		"Exit to windows",
	};
	
	private Color titleColor;
	private Font titleFont;
	public static AudioPlayer bgMusic;
	private Font font;
	int s=0;
	public GameCompleted(GameStateManager gsm) {
		
		this.gsm = gsm;
		
		try {
			bg = new Background("/Backgrounds/blackground.png", 1);
			titleColor = new Color(128, 0, 0);
			titleFont = new Font(
					"Century Gothic",
					Font.PLAIN,
					28);
			
			font = new Font("Arial", Font.PLAIN, 12);
			String name=JOptionPane.showInputDialog(Game.window,"Gratul�lunk!V�gigvitted a j�t�kot! K�rlek, add meg a neved: ");
			if (!name.equals("")) {
				db.sql_insert("INSERT INTO highscores (name,point,level,gem,datum,time) VALUES ('"+name+"','"+PlayerSave.killedDog+"','"+PlayerSave.currentLeveL+"','"+PlayerSave.gemCount+"',NOW(),'"+PlayerSave.time+"');");

				}else {}
				//HIGHSCORE LISTA
				db.sql_select("SELECT * FROM highscores ORDER BY point DESC"); // desck cs�kken�, order by rendez�s azaz sorba

				while (db.result.next()) { // felt�tle azt� van, hogy annyit adjoin ki amennyi megfelel a felt�tleknek
					s++;
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		
	}
	
	public void init() {
		bgMusic = new AudioPlayer("/Music/fanfare.mp3");
		bgMusic.play();
	}
	
	public void update() {
		bg.update();
	}
	
	public void draw(Graphics2D g) {
	
		bg.draw(g);
		// draw title
		g.setColor(titleColor);
		g.setFont(titleFont);
		g.drawString("Congratulation!",50, 70);
		g.drawString("You completed",50,110);		
		g.drawString(" the game!",80,130);
		
		// draw menu options
		g.setFont(font);
		for(int i = 0; i < options.length; i++) {
			if(i == currentChoice) {
				g.setColor(Color.WHITE);
			}
			else {
				g.setColor(Color.RED);
			}
			g.drawString(options[i], 120, 180 + i * 15);
		}
		
	}
	
	private void select() {
		if(currentChoice == 0) {
			gsm.setState(GameStateManager.HIGHSCORE);
		}
		if(currentChoice == 1) {
			gsm.setState(GameStateManager.MENUSTATE);
	
		}
		if(currentChoice ==2) {
			System.exit(0);
		}

	
	}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_ENTER){
			select();
		}
		if(k == KeyEvent.VK_UP) {
			currentChoice--;
			if(currentChoice == -1) {
				currentChoice = options.length - 1;
			}
		}
		if(k == KeyEvent.VK_DOWN) {
			currentChoice++;
			if(currentChoice == options.length) {
				currentChoice = 0;
			}
		}
	}
	public void keyReleased(int k) {}
	
}