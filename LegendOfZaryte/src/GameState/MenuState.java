package GameState;

import Entity.Player;
import Entity.PlayerSave;
import TileMap.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

public class MenuState extends GameState {
	
	private Background bg;
	private Background bg2;
	
	private int currentChoice = 0;
	private String[] options = {
		"Start",
		"Options",
		"HighScore",
		"Help",
		"Quit"
	};
	
	private Color titleColor;
	private Font titleFont;
	
	private Font font;
	public MenuState(GameStateManager gsm) {
		
		this.gsm = gsm;
		
		try {
			
			bg = new Background("/Backgrounds/menubg.gif", 1);
			bg.setVector(-0.2, 0);
			bg2 = new Background("/Backgrounds/mountains.gif",1);
			bg2.setVector(0,0);
			titleColor = new Color(128, 0, 0);
			titleFont = new Font(
					"Century Gothic",
					Font.PLAIN,
					28);
			
			font = new Font("Arial", Font.PLAIN, 12);
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void init() {}
	
	public void update() {
		bg.update();
		bg2.update();
	}
	
	public void draw(Graphics2D g) {
		
		// draw bg
		bg.draw(g);
		bg2.draw(g);
		
		// draw title
		g.setColor(titleColor);
		g.setFont(titleFont);
		g.drawString("Legend of Zaryte", 45, 70);
		
		// draw menu options
		g.setFont(font);
		for(int i = 0; i < options.length; i++) {
			if(i == currentChoice) {
				g.setColor(Color.WHITE);
			}
			else {
				g.setColor(Color.RED);
			}
			g.drawString(options[i], 135, 140 + i * 15);
		}
		
	}
	
	private void select() {
		if(currentChoice == 0) {
			Player.fullhp=3;
			Player.addFire=0;
			PlayerSave.addFire=0;
			PlayerSave.health=5;
			PlayerSave.maxHealth=5;
			PlayerSave.fire=1500;
			PlayerSave.maxFire=1500;
			PlayerSave.fireBallDamage =5;
			PlayerSave.scratchDamage=8;
			PlayerSave.scratchRange=40;
			PlayerSave.killedDog =0;
			PlayerSave.time =0;
			PlayerSave.gemCount=0;

			gsm.setState(GameStateManager.LEVEL1STATE);		
		}
		if(currentChoice == 1) {
			// options
			gsm.setState(GameStateManager.OPTIONSSTATE);
		}
		if(currentChoice == 2) {
		//highscore
			gsm.setState(GameStateManager.HIGHSCORE);

		}
		if(currentChoice == 3) {
			//help
			gsm.setState(GameStateManager.HELPSTATE);
		}
		if(currentChoice == 4) {
			System.exit(0);
		}
	}
	

	public void keyPressed(int k) {
		if(k == KeyEvent.VK_ENTER){
			select();
		}
		if(k == KeyEvent.VK_UP) {

			currentChoice--;
			if(currentChoice == -1) {
				currentChoice = options.length - 1;
			}
		}
		if(k == KeyEvent.VK_DOWN) {
			currentChoice++;
			if(currentChoice == options.length) {
				currentChoice = 0;
			}
		}
	}
	public void keyReleased(int k) {}
	
}
