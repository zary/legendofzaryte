package GameState;
import Main.GamePanel;
import Main.db;
import Audio.AudioPlayer;
import TileMap.*;
import Entity.*;
import Entity.Enemies.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.awt.Point;

import Entity.Enemy;
import Entity.Explosion;
import Entity.Player;
import Entity.PlayerSave;
import Entity.Teleport;
public class Level1State2 extends GameState {

	private TileMap tileMap;
	private Background bg;
	Random vel;
	
	private int currentChoice = 0;
	private String[] options = {
			"HighScore",
			"Exit to menu",
			"Exit to windows",
	};
	
	public static Player player;
	public static Teleport teleport;
	private ArrayList<Enemy> enemies,boss;
	private ArrayList<Enemy> gems;
	public static ArrayList<Explosion> explosions;
	public int healthBarWidth =50;
	public int healthBarHeight=5;
	private boolean pause=false;
	public int[] spawn=new int[2];
	Font font,font2;
	
	private HUD hud;
	private ArrayList<Enemy> lives;
	public static AudioPlayer bgMusic,bgMusic2;	
	public Level1State2(GameStateManager gsm) {
		this.gsm = gsm;
		init();
	}
	
	public void init() {
		PlayerSave.currentLeveL=4;
		tileMap = new TileMap(30);
		tileMap.loadTiles("/Tilesets/sotettile.png");
		tileMap.loadMap("/Maps/level1-2.map");
		tileMap.setPosition(0, 0);
		tileMap.setTween(0.5);

		bg = new Background("/Backgrounds/dirt2.png", 0.1);		
	
		
		spawn[0]=40;
		spawn[1]=30;
		
		font =new Font( "Arial", Font.PLAIN, 10);
		font2=new Font( "Arial", Font.PLAIN, 14);
			
		player = new Player(tileMap);
		player.setHealth(PlayerSave.getHealth());
		player.setLives(PlayerSave.getLives());
		player.setPosition(spawn[0],spawn[1]);// P�lya elej�n spawnol�s		
		player.setPosition(60,80);
		populateEnemies();
		populateGems();	
		hud = new HUD(player);
		explosions = new ArrayList<Explosion>();
		//music
		bgMusic = new AudioPlayer("/Music/level4.mp3"); 
		bgMusic2 = new AudioPlayer("/Music/fanfare.mp3"); 
		bgMusic.play();
		PlayerSave.ido.start();

	}
	private void select() {
		if(currentChoice == 0) {
			gsm.setState(GameStateManager.HIGHSCORE);
			bgMusic.stop();
		}
		if(currentChoice == 1) {
			gsm.setState(GameStateManager.MENUSTATE);
			bgMusic.stop();
		}
		if(currentChoice == 2) {
			System.exit(0);
		}
	}
	private void populateEnemies() {

		  enemies = new ArrayList<Enemy>();
		  boss=new ArrayList<Enemy>();
		  teleport = new Teleport(tileMap);
			lives=new ArrayList<Enemy>();
		  
			//Bunny Position and spawn
			Boss1 ny;
			Point[] nyPoint = new Point[] {
					new Point (2950,520)
					};
			for (int i=0;i < nyPoint.length;i++) {
				ny= new Boss1(tileMap, player,boss);
				ny.setPosition(nyPoint[i].x,nyPoint[i].y);
				boss.add(ny);	
			
			}
		//Bat Position and spawn
		Bat b;
		Point[] bPoint = new Point[db.sql_count("SELECT COUNT(*) FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Bat';")];
		db.sql_select("SELECT x,y FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Bat';");
		try {
			int i=0;
			while(db.result.next()) {
				int x1=db.result.getShort(1);
				int y1=db.result.getShort(2);
				bPoint[i]=new Point(x1,y1);
				i++;
			}
			
		} catch (SQLException e) {e.printStackTrace();}	
		for (int i=0;i < bPoint.length;i++) {
			b= new Bat(tileMap,player);
			b.setPosition(bPoint[i].x,bPoint[i].y);
			enemies.add(b);
		}
		//Spider Position and spawn
		Spider sp;
		Point[] sPoints = new Point[db.sql_count("SELECT COUNT(*) FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Spider';")];
		db.sql_select("SELECT x,y FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Spider';");
		try {
			int i=0;
			while(db.result.next()) {
				int x1=db.result.getShort(1);
				int y1=db.result.getShort(2);
				sPoints[i]=new Point(x1,y1);
				i++;
			}
			
		} catch (SQLException e) {e.printStackTrace();}
		for (int i=0;i < sPoints.length;i++) {
			sp= new Spider(tileMap);
			sp.setPosition(sPoints[i].x,sPoints[i].y);
			enemies.add(sp);
			if (i==0) sp.setY(280);
			if (i==1) sp.setY(580);
			if (i==2) sp.setY(550);
			if (i==3) sp.setY(370);
			if (i==4) sp.setY(130);
			if (i==5) sp.setY(130);
			if (i==6) sp.setY(610);
			if (i==7) sp.setY(430);
			if (i==8) sp.setY(310);
			if (i==9) sp.setY(130);
			if (i==10) sp.setY(520);
			if (i==11) sp.setY(520);
			if (i==12) sp.setY(520);
			if (i==13) sp.setY(370);
			if (i==14) sp.setY(400);
		}
		//Snail Position and spawn
		Slugger s;
		Point[] points = new Point[db.sql_count("SELECT COUNT(*) FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Slugger';")];
		db.sql_select("SELECT x,y FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Slugger';");
		try {
			int i=0;
			while(db.result.next()) {
				int x1=db.result.getShort(1);
				int y1=db.result.getShort(2);
				points[i]=new Point(x1,y1);
				i++;
			}
			
		} catch (SQLException e) {e.printStackTrace();}
		for (int i=0;i < points.length;i++) {
			s= new Slugger(tileMap);
			s.setPosition(points[i].x,points[i].y);
			enemies.add(s);
		}
	}
	private void populateGems() {
		
		gems = new ArrayList<Enemy>();
		
		Gems g;
		Point[] points = new Point[] {
				new Point (270,120),
				new Point (870,510),
				new Point (1020,60),
				new Point (1530,60),
				new Point (2250,300),
				new Point (2535,380)

		};
		for (int i=0;i < points.length;i++) {

			g= new Gems(tileMap);
			g.setPosition(points[i].x,points[i].y);
			g.gemChange(i);
			gems.add(g);
		}
		
	}	
	@SuppressWarnings("static-access")
	public void update() {
		bg.update();
		if(!pause) {
		PlayerSave.ido.start();
		// update player
		player.update();
		tileMap.setPosition(
			GamePanel.WIDTH / 2 - player.getx(),
			GamePanel.HEIGHT / 2 - player.gety()
		);
		
		//setbackground
		bg.setPosition(tileMap.getx(),tileMap.gety());	
		//bg2.update();
		
		//attack enemies
		player.checkAttack(enemies);
		player.checkAttack(boss);
		if (player.fullhp<=0) {
			bgMusic.stop();
			gsm.setState(GameStateManager.GAMEOVER);
			
		}else {
		
		//falling death
		if(player.gety() > tileMap.getHeight()) 	{
			player.fullhp--;
			player.health =PlayerSave.maxHealth;
			player.fire=PlayerSave.fire;
			player.setPosition(spawn[0],spawn[1]);
			Level2State2.player.setPosition(spawn[0],spawn[1]);		
		}
		//hit death
			if(player.health == 0) player.dead = true;

				if(player.dead)
				{			player.fullhp--;
			player.health =PlayerSave.maxHealth;
			player.fire=PlayerSave.fire;
			player.setPosition(spawn[0],spawn[1]);
			Level2State2.player.setPosition(spawn[0],spawn[1]);		

				}
				player.dead = false;
		
		}for (int i=0;i<boss.size();i++) {
			Enemy ny=boss.get(i);
			ny.update();
			if (ny.isDead()){ 
				boss.remove(i);
				player.setHealth(PlayerSave.getHealth());
				player.setLives(PlayerSave.getLives());
				bgMusic2.play();
				teleport.setPosition(2900, 520);
				}
			}
			if(teleport.intersects(player)) {
				PlayerSave.fullhp=player.fullhp;
				PlayerSave.health=player.health;
				gsm.setState(8);
			bgMusic.stop();
			bgMusic2.stop();
			PlayerSave.currentLeveL++;
			}
				
				
		//update enemies
			for (int i=0; i< enemies.size();i++ ) {
				Enemy e = enemies.get(i);
				e.update();
				if (e.isDead()) {
					enemies.remove(i);
					i--;
					explosions.add(new Explosion(e.getx(),e.gety()));	
					PlayerSave.increaseKilledDog();
					int v=vel.nextInt(100);
					if (v<40){ //40% es�ly h a d�gb�l �let j�jj�n ki
					Lives l= new Lives(tileMap);
					l.setPosition(e.getx(),e.gety());
					lives.add(l);
					}

				}
			}
			//update lives
			for (int i=0; i< lives.size();i++ ) {
				Enemy li = lives.get(i);
				li.update();
				if (li.intersects(player)) {
					lives.remove(i);
					if (Player.health<Player.maxHealth) {
						Player.health++;
					}
				}
			}	
		//update gems
		for (int i=0; i< gems.size();i++ ) {
			Enemy g = gems.get(i);
			g.update();
			if (g.intersects(player)) {
				if (i==0) {player.sfx.get("gem").play();hud.Gem1HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.maxHealth+=3;Player.maxHealth=PlayerSave.maxHealth; Player.health=PlayerSave.maxHealth; } //i a gem sz�ma, 0 az els�
				if (i==1) {player.sfx.get("gem").play();hud.Gem2HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.maxFire+=500;Player.maxFire=PlayerSave.maxFire;}
				if (i==2) {player.sfx.get("gem").play();hud.Gem3HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.fireBallDamage+=5;Player.fireBallDamage=PlayerSave.fireBallDamage;}
				if (i==3) {player.sfx.get("gem").play();hud.Gem4HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.scratchDamage+=2;Player.scratchDamage+=5;PlayerSave.scratchRange+=5;Player.scratchRange=PlayerSave.scratchRange;}
				if (i==4) {player.sfx.get("gem").play();hud.Gem5HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.addFire+=0.3;Player.addFire=PlayerSave.addFire;}
				if (i==5) {spawn[0]=g.getx();spawn[1]=g.gety();}
				
			}
		}
		//update explosion
		for (int i=0; i< explosions.size(); i++) {
			explosions.get(i).update();
			if (explosions.get(i).shouldRemove()){
				explosions.remove(i);
				i--;				
			}
		}	
	}	
			teleport.update();
		}
	@SuppressWarnings("static-access")
	public void draw(Graphics2D g) {
		
		// draw bg
		bg.draw(g);
		
		// draw tilemap
		tileMap.draw(g);
		
		// draw player
		player.draw(g);	
		
		//draw enemies
		for (int i=0;i < enemies.size();i++) {
			Enemy e = enemies.get(i);
			e.update();
			float healthScale = (float)e.health / (float)e.maxHealth;
			//HP bar for enemies
			if (Player.toggleHPbar) {
				g.setColor(Color.red);
				g.fillRect((int) ((int)e.getx()+TileMap.getx()-25),(int) (e.gety()+TileMap.gety()-25),healthBarWidth,healthBarHeight);
			    g.setColor(Color.GREEN);
			    g.fillRect((int) (e.getx()+TileMap.getx()-25),(int) (e.gety()+TileMap.gety()-25), (int) (healthBarWidth * healthScale), healthBarHeight);
				
			}enemies.get(i).draw(g);
				}

		//draw boss
		for(int i=0;i<boss.size();i++) {
			Enemy b = boss.get(i);
			b.update();
			float healthScale =(float) b.health / (float)b.maxHealth;
		    g.setColor(Color.GREEN);
		    g.fillRect((int) (b.getx()+TileMap.getx()-25),(int) (b.gety()+TileMap.gety()-25),(int) (healthBarWidth * healthScale), healthBarHeight);
			boss.get(i).draw(g);
		}
		//draw gempa
				for (int i=0;i < gems.size();i++) {
					gems.get(i).draw(g);
						}	
				
		//draw explosion
		for (int i =0;i< explosions.size();i++) {
			explosions.get(i).setMapPosition((int)tileMap.getx(), (int)tileMap.gety());
			explosions.get(i).draw(g);
		}
		//draw hud
		hud.draw(g);
		g.setFont(font2);
		g.setColor(Color.WHITE);
		Date d = new Date(PlayerSave.time * 1000);
		DateFormat df = new SimpleDateFormat("m:ss");
		g.drawString(String.valueOf(df.format(d)),GamePanel.WIDTH-40, GamePanel.HEIGHT-205);
	
		//drawing teleport
		teleport.draw(g);
		//drawing pause
		if (pause) {
			PlayerSave.ido.stop();
			Color alpha = new Color(0,0,0,128);
			g.setColor(alpha);
			g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
			g.setFont( font2);
			g.setColor(Color.RED);
			g.drawString("PRESS ESC TO CONTINUE", GamePanel.WIDTH/2-80, GamePanel.HEIGHT/8 );
			g.drawString("If you check the highscore,or exit to menu", GamePanel.WIDTH/2-120, GamePanel.HEIGHT/8+30 );
			g.drawString("you'll have to start over the game!!", GamePanel.WIDTH/2-100, GamePanel.HEIGHT/8+45 );
			for(int i = 0; i < options.length; i++) {
				if(i == currentChoice) {
					g.setColor(Color.WHITE);
				}
				else {
					g.setColor(Color.RED);
				}
				g.drawString(options[i], 120, 140 + i * 15);
			}

		}
		}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(true);
		if(k == KeyEvent.VK_RIGHT) player.setRight(true);
		if(k == KeyEvent.VK_UP) player.setUp(true);
		if(k == KeyEvent.VK_DOWN) player.setDown(true);
		if(k == KeyEvent.VK_W) player.setJumping(true);
		if(k == KeyEvent.VK_E) player.setGliding(true);
		if(k == KeyEvent.VK_R) player.setScratching();
		if(k == KeyEvent.VK_F) player.setFiring();
		if(k== KeyEvent.VK_ESCAPE){ 
			if (!pause) {pause=true;}
			else pause=false;
			}
		if(k == KeyEvent.VK_ENTER){
			select();
		}
		if(k == KeyEvent.VK_UP) {
			currentChoice--;
			if(currentChoice == -1) {
				currentChoice = options.length - 1;
			}
		}
		if(k == KeyEvent.VK_DOWN) {
			currentChoice++;
			if(currentChoice == options.length) {
				currentChoice = 0;
			}
		}
		if(k == KeyEvent.VK_0){ 
			if (!Player.toggleInfo) {Player.toggleInfo=true;}
			else Player.toggleInfo=false;
			}
		if(k == KeyEvent.VK_1){ 
			if (!Player.toggleHPbar) {Player.toggleHPbar=true;}
			else Player.toggleHPbar=false;
			}

	}
	
	public void keyReleased(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(false);
		if(k == KeyEvent.VK_RIGHT) player.setRight(false);
		if(k == KeyEvent.VK_UP) player.setUp(false);
		if(k == KeyEvent.VK_DOWN) player.setDown(false);
		if(k == KeyEvent.VK_W) player.setJumping(false);
		if(k == KeyEvent.VK_E) player.setGliding(false);
	}
	
}
