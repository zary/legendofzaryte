package GameState;

public class GameStateManager {
	

	private GameState[] gameStates;
	private int currentState;

	
	public static final int MENUSTATE = 0;
	public static final int	LEVEL1STATE = 1;//1
	public static final int OPTIONSSTATE = 2;
	public static final int HELPSTATE = 3;
	public static final int LEVEL1STATE2=4;	//4
	public static final int LEVEL2STATE1 =5;//2
	public static final int LEVEL2STATE2 =6;//3
	public static final int GAMEOVER =7;
	public static final int GAMECOMPLETED =8;
	public static final int HIGHSCORE =9;
	
	
	public GameStateManager() {
		
		gameStates = new GameState[10];
		
		currentState=MENUSTATE;
		loadState(currentState);
		
	}
	
	private void loadState(int state) {
		if(state == MENUSTATE) {
			gameStates[state] = new MenuState(this);}
			if (state == LEVEL1STATE){
				gameStates[state] = new Level1State(this);}
			if(state == OPTIONSSTATE){
				gameStates[state] = new OptionsState(this);}
			if (state == HELPSTATE){
				gameStates[state] = new HelpState(this);}
			if (state == LEVEL1STATE2){
				gameStates[state] = new Level1State2(this);}
			if (state == LEVEL2STATE1){
				gameStates[state] = new Level2State1(this);	}
			if (state == LEVEL2STATE2){
				gameStates[state] = new Level2State2(this);	}
			if (state == GAMEOVER){
				gameStates[state] = new GameOver(this);	}
			if (state == GAMECOMPLETED){
				gameStates[state] = new GameCompleted(this);	}
			if (state == HIGHSCORE){
				gameStates[state] = new HighScore(this);	}
	}
	
	private void unloadState (int state) {
		gameStates[state] = null;
	}
	
	public void setState (int state) {
		unloadState(currentState);
		currentState = state;
		loadState(currentState);

	}
	public void update() {
		try {

		gameStates[currentState].update();
		}
		catch(Exception e ) {}
	}

	public void draw (java.awt.Graphics2D g) {
		try {
		gameStates[currentState].draw(g);
		}
		catch (Exception e) {}
		
		
	}
	public void keyPressed (int k){
		gameStates[currentState].keyPressed(k);
	}
	public void keyReleased(int k){
		gameStates[currentState].keyReleased(k);
	}
	
}
