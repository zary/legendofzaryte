package GameState;

import Main.db;
import TileMap.Background;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HighScore extends GameState {
	
	private Background bg;

	private int currentChoice = 0;
	private String[] options = {
		"Back to menu",
		"Exit to windows",
	};
	
	private Color titleColor;
	private Font titleFont;
	
	private Font font;

	
	public HighScore(GameStateManager gsm) {
		
		this.gsm = gsm;
		
		try {
			bg = new Background("/Backgrounds/blackground.png", 1);
			titleColor = new Color(128, 0, 0);
			titleFont = new Font(
					"Century Gothic",
					Font.PLAIN,
					28);	
			font = new Font("Arial", Font.PLAIN, 12);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void init() {}
	
	public void update() {
		bg.update();
	}

	public void draw(Graphics2D g ) {

		bg.draw(g);
		// draw title
		g.setColor(titleColor);
		g.setFont(titleFont);
		// draw menu options
		g.setFont(font);
		for(int i = 0; i < options.length; i++) {
			if(i == currentChoice) {
				g.setColor(Color.WHITE);
			}
			else {
				g.setColor(Color.RED);
			}
			g.drawString(options[i], 120, 200 + i * 15);
		}
		
		try {
			bg = new Background("/Backgrounds/blackground.png", 1);
			titleColor = new Color(128, 0, 0);
			titleFont = new Font(
					"Century Gothic",
					Font.PLAIN,
					28);
			
			font = new Font("Arial", Font.PLAIN, 12);
			//HIGHSCORE LISTA
			db.sql_select("SELECT * FROM highscores ORDER BY point DESC,gem DESC,time ASC"); // desck cs�kken�, order by rendez�s azaz sorba
			int s=0;
			while (db.result.next() && s!=8) { // felt�tle azt� van, hogy annyit adjoin ki amennyi megfelel a felt�tleknek
				s++;
				//System.out.println(s+" ");
				g.setColor(Color.RED);
				g.drawString("Name", 0,10);
				g.drawString("Kills", 60,10);
				g.drawString("Level", 110,10);
				g.drawString("Time", 170,10);
				g.drawString("Gems", 200,10);
				g.drawString("Date", 280,10);
  
				g.drawString(db.result.getString("name")+" ",0,15+s*20); // db.result.getString("name")   <-�rt�kl
				g.drawString(db.result.getString("point")+" ",70,15+s*20);
				g.drawString(db.result.getString("level")+" ",130,15+s*20);
				g.drawString(db.result.getString("gem")+" ",210,15+s*20);
				
				Date d = new Date(db.result.getShort("time") * 1000);
				DateFormat df = new SimpleDateFormat("m:ss");
				g.drawString(String.valueOf(df.format(d))+" ",170,15+s*20);
				g.drawString(db.result.getString("datum")+" ",250,15+s*20);
			
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	 finally {
	    try { db.result.close(); } catch (Exception e) { /* ignored */ }
	    try { db.query.close(); } catch (Exception e) { /* ignored */ }
	    try { db.conn.close(); } catch (Exception e) { /* ignored */ }
	}
		
	}
	
	private void select() {
		if(currentChoice == 0) {
			gsm.setState(GameStateManager.MENUSTATE);
	
		}
		if(currentChoice == 1) {
			System.exit(0);
		}

	
	}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_ENTER){
			select();
		}
		if(k == KeyEvent.VK_UP) {
			currentChoice--;
			if(currentChoice == -1) {
				currentChoice = options.length - 1;
			}
		}
		if(k == KeyEvent.VK_DOWN) {
			currentChoice++;
			if(currentChoice == options.length) {
				currentChoice = 0;
			}
		}
	}
	public void keyReleased(int k) {}
	
}
