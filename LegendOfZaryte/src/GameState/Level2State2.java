package GameState;
import Main.GamePanel;
import Main.db;
import Audio.AudioPlayer;
import TileMap.*;
import Entity.*;
import Entity.Enemies.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.awt.Point;

import Entity.Enemy;
import Entity.Player;
import Entity.PlayerSave;
import Entity.Teleport;
public class Level2State2 extends GameState {
	Random vel;	
	
	private int currentChoice = 0;
	private String[] options = {
			"HighScore",
			"Exit to menu",
			"Exit to windows",
	};
	
	private TileMap tileMap;
	private Background bg,bg2,bg3;
	
	public static Player player;
	public static Teleport teleport;
	
	private ArrayList<Enemy> enemies;
	private ArrayList<Enemy> gems;
	private ArrayList<Enemy> lives;
	public static ArrayList<Explosion> explosions;
	
	public int healthBarWidth =50;
	public int healthBarHeight=5;
	private boolean pause=false;
	
	private HUD hud;
	
	Font font,font2;
	public static AudioPlayer bgMusic;
	
	public int[] spawn=new int[2];
	public static int[] Espawn=new int[2];
	public Level2State2(GameStateManager gsm) {
		this.gsm = gsm;
		init();
	}
	
	public void init() {
		PlayerSave.currentLeveL=3;
		vel=new Random();
		tileMap = new TileMap(30);
		tileMap.loadTiles("/Tilesets/havastile.png");
		tileMap.loadMap("/Maps/level2-2.map");
		tileMap.setPosition(0, 0);
		tileMap.setTween(0.5);
		spawn[0]=350;
		spawn[1]=110; 
		Espawn[0]=300;
		Espawn[1]=300;
		bg = new Background("/Backgrounds/grassbg3.gif", 0.001);
		bg2 = new Background("/Backgrounds/havazas2.gif", 0.1);
		bg3 = new Background("/Backgrounds/havazas2.gif", 0.1);
		bg2.setVector(-0.001, 0.2);
		bg3.setVector(0.001, 0.3);

	    	
		player = new Player(tileMap);	
		player.setPosition(spawn[0],spawn[1]); 	// Spawn Point for Player
		
		populateEnemies();
		populateGems();
		explosions = new ArrayList<Explosion>();
		
		hud = new HUD(player);
		font =new Font( "Arial", Font.PLAIN, 14);
		font2=new Font( "Arial", Font.PLAIN, 14);
		
		//music
		bgMusic = new AudioPlayer("/Music/level3.mp3");
		bgMusic.play();
		PlayerSave.ido.start();
	}
	private void select() {
		if(currentChoice == 0) {
			gsm.setState(GameStateManager.HIGHSCORE);
			bgMusic.stop();
		}
		if(currentChoice == 1) {
			gsm.setState(GameStateManager.MENUSTATE);
			bgMusic.stop();
		}
		if(currentChoice == 2) {
			System.exit(0);
		}
	}
	private void populateEnemies() {
		enemies = new ArrayList<Enemy>(); 
		teleport = new Teleport(tileMap);
		lives=new ArrayList<Enemy>();
		
		HavasBird hb;
		Point[] HbPoint = new Point[] {

				new Point (Espawn[0],Espawn[1]),		
				};			
		for (int i=0;i < HbPoint.length;i++) {
			hb= new HavasBird(tileMap,player);
			hb.setPosition(HbPoint[i].x,HbPoint[i].y);
			enemies.add(hb);
		}
		
		//Bird Positions and spawn
		Bird b;
		Point[] bPoint = new Point[db.sql_count("SELECT COUNT(*) FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Bird';")];
		db.sql_select("SELECT x,y FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Bird';");
		try {
			int i=0;
			while(db.result.next()) {
				int x1=db.result.getShort(1);
				int y1=db.result.getShort(2);
				bPoint[i]=new Point(x1,y1);
				i++;
			}
			
		} catch (SQLException e) {e.printStackTrace();}		
		for (int i=0;i < bPoint.length;i++) {
			b= new Bird(tileMap,player);
			b.setPosition(bPoint[i].x,bPoint[i].y);
			enemies.add(b);
		}
		//Spider Positions and spawn
		Spider sp;	
		Point[] sPoints = new Point[db.sql_count("SELECT COUNT(*) FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Spider';")];
		db.sql_select("SELECT x,y FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Spider';");
		try {
			int i=0;
			while(db.result.next()) {
				int x1=db.result.getShort(1);
				int y1=db.result.getShort(2);
				sPoints[i]=new Point(x1,y1);
				i++;
			}
			
		} catch (SQLException e) {e.printStackTrace();}
		for (int i=0;i < sPoints.length;i++) {
			sp= new Spider(tileMap);
			sp.setPosition(sPoints[i].x,sPoints[i].y);
			enemies.add(sp);
			if (i==0) sp.setY(110);
			if (i==1) sp.setY(110);
			if (i==2) sp.setY(140);
			if (i==3) sp.setY(200);
			if (i==4) sp.setY(350);
			if (i==5) sp.setY(170);
 			if (i==6) sp.setY(130);
		}	
		
		SnowSpider ss;
		Point[] Gspoint = new Point[] {
				new Point(2600,350)
				};
		for (int i=0;i < Gspoint.length;i++) {
			ss= new SnowSpider(tileMap);
			ss.setPosition(Gspoint[i].x,Gspoint[i].y);
			enemies.add(ss);
		}
		//Snail Positions and spawn
		Slugger s;
		Point[] points = new Point[db.sql_count("SELECT COUNT(*) FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Slugger';")];
		db.sql_select("SELECT x,y FROM enemies,enemy_types WHERE enemies.type=enemy_types.id AND enemies.level='"+PlayerSave.currentLeveL+"'AND enemy_types.name='Slugger';");
		try {
			int i=0;
			while(db.result.next()) {
				int x1=db.result.getShort(1);
				int y1=db.result.getShort(2);
				points[i]=new Point(x1,y1);
				i++;
			}
			
		} catch (SQLException e) {e.printStackTrace();}
		
		
		for (int i=0;i < points.length;i++) {
			s= new Slugger(tileMap);
			s.setPosition(points[i].x,points[i].y);
			enemies.add(s);
		}
	}
	private void populateGems() {
		
		gems = new ArrayList<Enemy>();
		
		//Gem Positions
		Gems g;
		Point[] points = new Point[] {
				new Point (520,350),
				new Point (960,170),
				new Point (1050,80),
				new Point (2360,350),
				new Point (3165,80),
				new Point(2130,200), //minding a hatodik a cecspoint
		};
		for (int i=0;i < points.length;i++) {

			g= new Gems(tileMap);
			g.setPosition(points[i].x,points[i].y);
			g.gemChange(i);
			gems.add(g);
		}
		
	}
	@SuppressWarnings("static-access")
	public void update() {
		
		if (!pause) {
		PlayerSave.ido.start();
		bg.update();bg3.update();
		bg2.update();
		
		// update player
		player.update();
		tileMap.setPosition(
			GamePanel.WIDTH / 2 - player.getx(),
			GamePanel.HEIGHT / 2 - player.gety()
		);
		
		//setbackground
		bg.setPosition(tileMap.getx(),tileMap.gety());	
			
		//attack enemies
		player.checkAttack(enemies);
		if (player.fullhp==0) {
			bgMusic.stop();
			gsm.setState(GameStateManager.GAMEOVER);
			
		}else {
		
			//falling death
			if(player.gety() > tileMap.getHeight()) 	{
				player.fullhp--;
				player.health =PlayerSave.maxHealth;
				player.fire=PlayerSave.fire;
				player.setPosition(spawn[0],spawn[1]);
				Level2State2.player.setPosition(spawn[0],spawn[1]);
			}
			//hit death
				if(player.health == 0) player.dead = true;

					if(player.dead)
					{				
						player.fullhp--;
						player.health =PlayerSave.maxHealth;
						player.fire=PlayerSave.fire;
						player.setPosition(spawn[0],spawn[1]);
						Level2State2.player.setPosition(spawn[0],spawn[1]);
					}
					player.dead = false;
		}

		//update enemies
		for (int i=0; i< enemies.size();i++ ) {
			Enemy e = enemies.get(i);
			e.update();
			if (e.isDead()) {
				enemies.remove(i);
				i--;
				explosions.add(new Explosion(e.getx(),e.gety()));	
				PlayerSave.increaseKilledDog();
				int v=vel.nextInt(100);
				if (v<35){ //35% es�ly h a d�gb�l �let j�jj�n ki
				Lives l= new Lives(tileMap);
				l.setPosition(e.getx(),e.gety());
				lives.add(l);
				}

			}
		}

				teleport.setPosition(3150, 350);
				if(teleport.intersects(player)) {
					PlayerSave.fullhp=player.fullhp;
					PlayerSave.health=player.health;
					gsm.setState(4);
				bgMusic.stop();}
				
		//update gems
		for (int i=0; i< gems.size();i++ ) {
			Enemy g = gems.get(i);
			g.update();
			
			if (g.intersects(player)) {		
				if (i==0) {player.sfx.get("gem").play();hud.Gem1HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.maxHealth+=3;Player.maxHealth=PlayerSave.maxHealth; Player.health=PlayerSave.maxHealth; } //i a gem sz�ma, 0 az els�
				if (i==1) {player.sfx.get("gem").play();hud.Gem2HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.maxFire+=500;Player.maxFire=PlayerSave.maxFire;}
				if (i==2) {player.sfx.get("gem").play();hud.Gem3HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.fireBallDamage+=5;Player.fireBallDamage=PlayerSave.fireBallDamage;}
				if (i==3) {player.sfx.get("gem").play();hud.Gem4HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.scratchDamage+=2;Player.scratchDamage=PlayerSave.scratchDamage;PlayerSave.scratchRange+=5;Player.scratchRange=PlayerSave.scratchRange;}
				if (i==4) {player.sfx.get("gem").play();hud.Gem5HUD();g.setPosition(0,0);PlayerSave.gemCount++;PlayerSave.addFire+=0.2;Player.addFire=PlayerSave.addFire;}
				if (i==5) {spawn[0]=g.getx();spawn[1]=g.gety();}	
			}
		}
		//update lives
				for (int i=0; i< lives.size();i++ ) {
					Enemy li = lives.get(i);
					li.update();
					if (li.intersects(player)) {
						lives.remove(i);
						if (Player.health<Player.maxHealth) {
							Player.health++;
							System.out.println("Curr: "+PlayerSave.health+" MAX: "+PlayerSave.maxHealth);
						}
					}
				}
		//update explosion
		for (int i=0; i< explosions.size(); i++) {
			explosions.get(i).update();
			if (explosions.get(i).shouldRemove()){
				explosions.remove(i);
				i--;		
			}
		}
		//teleport update
		teleport.update();
		}
	}
	public void draw(Graphics2D g) {
		
		//draw bg
		bg.draw(g);
		bg2.draw(g);
			
		// draw tilemap
		tileMap.draw(g);
				
		// draw player
		player.draw(g);	
			bg3.draw(g);

		//draw enemies
		for (int i=0;i < enemies.size();i++) {
			Enemy e = enemies.get(i);
			e.update();
			float healthScale = (float)e.health / (float)e.maxHealth;
			//HP bar for enemies
			if (Player.toggleHPbar) {
				g.setColor(Color.red);
				g.fillRect((int) ((int)e.getx()+TileMap.getx()-25),(int) (e.gety()+TileMap.gety()-25),healthBarWidth,healthBarHeight);
			    g.setColor(Color.GREEN);
			    g.fillRect((int) (e.getx()+TileMap.getx()-25),(int) (e.gety()+TileMap.gety()-25), (int) (healthBarWidth * healthScale), healthBarHeight);
				
			}
			enemies.get(i).draw(g);
				}	
					
		//draw gempa
				for (int i=0;i < gems.size();i++) {
					gems.get(i).draw(g);
						}	
				
		//draw lives
		for (int i=0;i < lives.size();i++) {
				lives.get(i).draw(g);
					}			
		//draw explosion
		for (int i =0;i< explosions.size();i++) {
			explosions.get(i).setMapPosition((int)TileMap.getx(), (int)TileMap.gety());
			explosions.get(i).draw(g);
		}
		//draw hud
		hud.draw(g);
		g.setFont(font2);
		g.setColor(Color.WHITE);
		Date d = new Date(PlayerSave.time * 1000);
		DateFormat df = new SimpleDateFormat("m:ss");
		g.drawString(String.valueOf(df.format(d)),GamePanel.WIDTH-40, GamePanel.HEIGHT-205);
		
		//drawing teleport
		teleport.draw(g);
		if (pause) {
			PlayerSave.ido.stop();
			Color alpha = new Color(0,0,0,128);
			g.setColor(alpha);
			g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
			g.setFont( font2);
			g.setColor(Color.RED);
			g.drawString("PRESS ESC TO CONTINUE", GamePanel.WIDTH/2-80, GamePanel.HEIGHT/8 );
			g.drawString("If you check the highscore,or exit to menu", GamePanel.WIDTH/2-120, GamePanel.HEIGHT/8+30 );
			g.drawString("you'll have to start over the game!!", GamePanel.WIDTH/2-100, GamePanel.HEIGHT/8+45 );
			for(int i = 0; i < options.length; i++) {
				if(i == currentChoice) {
					g.setColor(Color.WHITE);
				}
				else {
					g.setColor(Color.RED);
				}
				g.drawString(options[i], 120, 140 + i * 15);
			}

		}
	}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(true);
		if(k == KeyEvent.VK_RIGHT) player.setRight(true);
		if(k == KeyEvent.VK_UP) player.setUp(true);
		if(k == KeyEvent.VK_DOWN) player.setDown(true);
		if(k == KeyEvent.VK_W) player.setJumping(true);
		if(k == KeyEvent.VK_E) player.setGliding(true);
		if(k == KeyEvent.VK_R) player.setScratching();
		if(k == KeyEvent.VK_F) player.setFiring();
		if(k== KeyEvent.VK_ESCAPE){ 
			if (!pause) {pause=true;}
			else pause=false;
			}
		if(k == KeyEvent.VK_ENTER){
			select();
		}
		if(k == KeyEvent.VK_UP) {
			currentChoice--;
			if(currentChoice == -1) {
				currentChoice = options.length - 1;
			}
		}
		if(k == KeyEvent.VK_DOWN) {
			currentChoice++;
			if(currentChoice == options.length) {
				currentChoice = 0;
			}
		}
		if(k == KeyEvent.VK_0){ 
			if (!Player.toggleInfo) {Player.toggleInfo=true;}
			else Player.toggleInfo=false;
			}
		if(k == KeyEvent.VK_1){ 
			if (!Player.toggleHPbar) {Player.toggleHPbar=true;}
			else Player.toggleHPbar=false;
			}
	}
	public void keyReleased(int k) {
		if(k == KeyEvent.VK_LEFT) player.setLeft(false);
		if(k == KeyEvent.VK_RIGHT) player.setRight(false);
		if(k == KeyEvent.VK_UP) player.setUp(false);
		if(k == KeyEvent.VK_DOWN) player.setDown(false);
		if(k == KeyEvent.VK_W) player.setJumping(false);
		if(k == KeyEvent.VK_E) player.setGliding(false);
	}
	
}