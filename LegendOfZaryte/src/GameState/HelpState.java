package GameState;

import TileMap.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

public class HelpState extends GameState {
	
	private Background bg;
	private Background bg2;
	private String title;
	
	private int currentChoice = 0;
	private String[] options = {
		"About",
		"Back"
	};
	
	private Color titleColor;
	private Font titleFont;
	
	private Font font;
	
	public HelpState(GameStateManager gsm) {
		title = "Help";
		this.gsm = gsm;
		
		try {
			
			bg = new Background("/Backgrounds/sunset.gif", 1);
			bg.setVector(-0.15, 0);
			bg2 = new Background("/Backgrounds/mountains.gif",1);
			bg2.setVector(0,0);
			titleColor = new Color(128, 0, 0);
			titleFont = new Font(
					"Century Gothic",
					Font.PLAIN,
					28);
			
			font = new Font("Arial", Font.PLAIN, 12);
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void init() {}
	
	public void update() {
		bg.update();
		bg2.update();
	}
	
	public void draw(Graphics2D g) {
		
		// draw bg
		bg.draw(g);
		bg2.draw(g);
		
		// draw title
		g.setColor(titleColor);
		g.setFont(titleFont);
		g.drawString(title,130, 70);
		
		// draw menu options
		g.setFont(font);
		for(int i = 0; i < options.length; i++) {
			if(i == currentChoice) {
				g.setColor(Color.WHITE);
			}
			else {
				g.setColor(Color.RED);
			}
			g.drawString(options[i], 130, 140 + i * 15);
		}
		
	}
	
	private void select() {
		if(currentChoice == 0) {
			//Gameplay
			JOptionPane.showMessageDialog(null,
				    "A j�t�kot k�sz�tette:\n"
				    + "Feh�r P�ter\n"
				    + " 2014.",
				    "Informations",
				    JOptionPane.INFORMATION_MESSAGE
				   );
			
		}
		if(currentChoice == 1) {
			// enemies
			gsm.setState(GameStateManager.MENUSTATE);
		}

	}
	
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_ENTER){
			select();
		}
		if(k == KeyEvent.VK_UP) {
			currentChoice--;
			if(currentChoice == -1) {
				currentChoice = options.length - 1;
			}
		}
		if(k == KeyEvent.VK_DOWN) {
			currentChoice++;
			if(currentChoice == options.length) {
				currentChoice = 0;
			}
		}
	}
	public void keyReleased(int k) {}
	
}
